<!doctype html>
<html lang="pt-br">
<title>{{default_from_name|safe|linebreaksbr}}</title>
<head>
    <meta charset="utf-8">
</head>
<body>

<p>{{voter.name}},</p>

<br>
<p>
A apuração da eleição <strong>{{election_name}}</strong> foi realizada e liberada:
<br>
<br>
  {{election_url}}
<br>
{{custom_message|safe|linebreaksbr}}
</p>
<br>
{% if voter.vote_hash %} O código de rastreamento de sua cédula nessa eleição foi:
<br>
<p>
  <em>{{voter.vote_hash}}</em>
<br>
<br>
Se você acha que esse rastreador está errado, por favor, entre em contato.
<br>
</p>
{% else %}
<p>
Parece que você não votou nessa eleição. Se você acha que votou, por favor, entre em contato.
</p>
{% endif %}
<br>
--
<br>
<font style="" size="1" face="verdana, sans-serif"><b style="">Instituto Federal do Rio Grande do Sul Campus Canoas</b></font>
<br>
<u><a href="https://ifrs.edu.br/canoas/" target="_blank" style=""><font style="" face="verdana, sans-serif">https://ifrs.edu.br/canoas/</font></a></u>

</body>
</html>

