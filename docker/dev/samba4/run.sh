#!/bin/bash
set -e

echo "nameserver samba4" > /etc/resolv.conf
echo "Samba4 provision...."
bin/samba-tool domain provision --server-role=dc --use-rfc2307 --dns-backend=SAMBA_INTERNAL --realm=AD.CAMPUS.IFRS.EDU.BR --domain=CAMPUS  --host-name=samba4 --adminpass=Passw0rd
cp /usr/local/samba/private/krb5.conf /etc/
echo "Importando..."
bin/ldbadd -H private/sam.ldb 1-ou-users-groups.ldif
bin/ldbmodify -H private/sam.ldb 2-member-groups.ldif
echo "Criando usuários, senha=senha5@"
echo "Usuário fulano.silva"
bin/samba-tool user setpassword fulano.silva --newpassword=senha5@
echo "Criando usuário ciclano.santos"
bin/samba-tool user setpassword ciclano.santos --newpassword=senha5@
echo "Criando usuários alunos"
for i in {1..2}
do
    bin/samba-tool user create aluno${i} senha5@ --given-name=Aluno${i} --surname=Alunosn --userou=OU=alunos --use-username-as-cn --mail-address=aluno${i}@campus.ifrs.edu.br
done
for i in {1..2}
do
    bin/samba-tool user create 0200${i} senha5@ --given-name=Aluno${i} --surname=Alunosn --userou=OU=alunos --use-username-as-cn --mail-address=0200${i}@campus.ifrs.edu.br
done
exec "$@"