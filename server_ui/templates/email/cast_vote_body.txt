<!doctype html>

<html lang="pt-br">
<title>{{default_from_name|safe}}</title>
<head>
    <meta charset="utf-8">
</head>
    <body>
        <p style="color: red">As informação neste e-mail são sigilosas, não responda ou encaminhe este e-mail isto garante o sigilo do seu voto</p>
        <p><strong>{{voter.name}}</strong>,
        <p>Seu voto foi depositado com sucesso para {{election.name}}</p>
        <p>O seu voto foi registrado com o seguinte código de confirmação:<br>
        <strong>
            {{cast_vote.vote_hash}}
        </strong>
        {% if election.use_voter_aliases %}
            <p> O seu pseudônimo de eleitor é: <strong>{{voter.alias}}</strong></p>
        {% endif %}
        <!-- TODO: fix hostname <p>O seu voto está guardado em: {{cast_vote_url}}</p> -->
        <p>O resultado da eleição será divulgado em horário e local conforme Edital.</p>     
--
        <br>
        <font style="" size="1" face="verdana, sans-serif"><b style="">Instituto Federal de Rio grande do Sul - Campus Canoas</b></font>
        <br>
        <u><a href="https://ifrs.edu.br/canoas/" target="_blank" style=""><font style="" face="verdana, sans-serif">https://ifrs.edu.br/canoas/</font></a>
        </u>
</body>
</html>
