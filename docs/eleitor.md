# Manual do Eleitor

## Importante

>  O Sistema de Votação permite que você deposite cédulas na urna quantas vezes desejar, porém somente a última cédula depositada é a que será contabilizada na apuração da urna e todas as cédulas anteriores serão descartadas.

> A autenticação no sistema é solicitada após avançar a etapa **Revisão do voto**

## Acessar a votação :id=acessar-votacao

O endereço para acesso a votação se encontra no Edital e/ou nas notícias do site oficial do campus.

### Tela Inicial

A página inicial da cabine de votação contém o título da eleição, instruções básicas e o código identificador da eleição no rodapé, para continuar clique no botão **Iniciar**.

![Tela inicial da cabine de votação](/img/eleitor-cabine.png)


## Escolher candidato(a) :id=escolher-candidato

Escolha o candidato(a) e clique no botão **Próxima Passo**, para trocar sua escolha primeiro desmarque a opção escolhida e selecione outra opção, você também pode escolher a opção em Branco ou Nulo caso disponível. 

Opcionalmente o candidato(a) pode disponibilizar um link externo para mostrar seu perfil e propostas, clique em **Mais informações** ao lado do nome de cada candidato(a).

> Há eleições com uma ou mais questões, a quantidade de questões é mostrada conforme a imagem, neste caso possui apenas uma questão **Questão 1 de 1**

!> Fique atento a quantidade mínima e máxima de condidato(a)s que você pode escolher, há eleições em que é possível escolher um ou mais candidato(a)s, conforme a imagem abaixo esta eleição é obrigatório a escolha de uma opção, seja candidato(a) ou voto em branco. A quantidade mínima e máxima é definida pela Comissão Eleitoral e descrita no Edital.

![Tela de votação e escolha do candidato](/img/eleitor-candidatos.png)

## Revisar o voto :id=revisar-voto

É possível revisar as suas escolhas de voto e caso esteja tudo correto clique no botão **Depositar esta Cédula na Urna**.

Se desejar alterar o voto, basta clicar no link **Editar resposta(s)** e a tela retornará para a cédula de votação com todos os candidatos disponíveis para nova escolha.

!> Antes de depositar sua cédula anote o código rastreador.

![Tela de revisão do voto](/img/eleitor-revisao.png)

## Autenticar-se no sistema :id=autenticar-sistema

Nesta etapa é solicitado as suas credenciais de acesso ao Sistema de Votação no qual utiliza Login Único que consiste no login e senha igual a todos os sistemas do campus Canoas, ou seja, para aluno(a)s o login é o número de matrícula ( Ex.: 02010001) e para o(a)s servidore(a)s é o nome e sobrenome (Ex.: nome.sobrenome).

Clique no botão **Conectar com meu usuário e senha do IFRS - Campus Canoas**, insira o seu login e senha e clique no botão **Conectar** para avançar a esta etapa.

!> Leia [Responsabilidades do Eleitor](#responsabilidades-eleitor)

![Tela para informar que é necessário login via IFRS - Campus Canoas](/img/eleitor-autenticacao.png)

![Tela de login](/img/eleitor-login.png)

## Depositar cédula :id=depositar-cedula

Esta é a etapa final, verifique se seu nome é mostrado na tela e escolha a opção de **Confirmar** para depositar sua cédula ou **Cancelar** para cancelar todo o processo e acessar a votação novamente desde o início.

> Ao clicar em **Confirmar** ou **Cancelar** por segurança o sistema força o seu logout.

!> Após a confirmação do depósito da cédula, anote o código rastreador da cédula e feche o navegador.

![Tela de confirmação do voto](/img/eleitor-confirmacao.png)

![Tela confirmação do deposito da cédula](/img/eleitor-deposito.png)

## Acompanhar a eleição e registro da cédula :id=acompanhar-eleicao

A Comissão Eleitoral mantém as informações da eleição publicada no site oficial do *campus* Canoas incluindo o endereço da eleição de acesso ao público para acompanhamento sem a necessidade de realizar login no sistema, neste endereço é possível ver a lista eleitores, as cédulas já depositadas em sua forma anônima para verificar se sua cédula foi registrada. Após da apuração dos votos, o resultado final também torna-se disponível através do enredeço da eleição.

![Resultado da eleição](/img/eleitor-resultado.png)

### Verificar o registro da cédula

Para verificar o registro de sua cédula acesse o [endereço da eleição](#acompanhar-eleicao) fornecido pela Comissão Eleitoral, clique em **Eleitores & Cédulas** e faça uma busca pelo código rastreador que você anotou após [depositar sua cédula na cabine de votação](#depositar-cedula).

Também é possível verificar se a cédula foi contabilizada após a liberação do resultado final da apuração dos votos, quando o resultado estiver disponível clique em **Conferir a apuração da eleição**, em seguida **Iniciar verificação** e faça uma busca pelo seu código rastreador.

> Não é possível ter acesso ao voto em texto claro, ou seja, descriptografado [saiba mais ...](/#sobre)

### Resultado da eleição

Após concluída todas as etapas da eleição a Comissão Eleitoral realiza a apuração dos votos via sistema e libera o resultado que ficará disponível no [endereço da eleição](#acompanhar-eleicao)

## Responsabilidades do Eleitor :id=responsabilidades-eleitor

 - Anotar o código rastreador após [depositar a cédula](#depositar-cedula)
 - Certificar-se previamente que suas credenciais de acesso aos sistemas do *campus* Canoas estão normais acessando https://sifrs.canoas.ifrs.edu.br

## Suporte

Todo o suporte de primeiro nível e dado pela Comissão Eleitoral do *campus* Canoas, contato comissao.eleitoral@canoas.ifrs.edu.br .
 