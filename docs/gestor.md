# Manual do Gestor da Eleição

## Acessar painel do gestor :id=acesso-gestor

Acesse o sistema https://vote.canoas.ifrs.edu.br, clique em **Conectar** e insira suas credenciais de acesso que consiste no login e senha igual a todos os sistemas do campus Canoas, na tela inicial do painel de gestor deve aparecer um botão **Criar eleição** entre outras informações.

> Se o botão **Criar eleição** não estiver disponível significa que você não tem permissão de Gestor, consulte [**solicitar permissão**](#solicitar-permissao)

## Criar eleição :id=criar-eleicao

Na página inicial do painel de gestor clique em **Criar eleição**, preencha os campos necessários e clique em **Próximo**

> Em uma eleição onde cada segmento, discente, docente e técnico administrativo votam em seus pares, é necessário criar uma eleição por segmento.

### Recomendações :id=recomendacoes
!>O sistema disponibiliza alguns formatos de eleição, afim de  padronizar as eleições e possibilitar que os eleitore(a)s utilizem seu login e senha padrão do *Campus* Canoas siga as seguintes recomendações

- Preencher o campo **Endereço de email para ajuda** com o e-mail da Comissão Eleitoral comissao.eleitoral@canoas.ifrs.edu.br

<span style="color:red">Desabilitar</span> as opções: 
- <span style="color:red">Privada</span>
- <span style="color:red">Usar funcionalidade avançada de auditoria</span>

<span style="color:green">Habilitar</span> as opções: 
- <span style="color:green">Usar pseudônimos de eleitores</span>
- <span style="color:green">Tornar ordem das questões aleatória</span>

Demais campos o gestor preenche conforme a eleição.


### Descrição completa das opções :id=criar-eleicao-opcoes

| **Campo**                                     | **Descrição**                                                |
| --------------------------------------------- | ------------------------------------------------------------ |
| **Nome abreviado**                            | Será o sufixo na URL da eleição e **não pode** conter espaços, acentos ou caracteres especiais. Ex. Representante2020. |
| **Nome**                                      | O nome apresentável para a sua eleição que ficará em destaque para os eleitores, com o **máximo de 250 caracteres**. Ex. Eleição de Representante 2020 |
| **Descrição**                                 | Informações detalhadas sobre a eleição com o máximo de 4000 caracteres (podem ser usadas algumas tags HTML). Ex. Datas importantes do cronograma, links para portarias, editais, etc. |
| **Usar pseudônimos de eleitores**             | Se marcada, a identidade dos eleitores será substituída por pseudônimos, p. ex.: "V12, V13, V14", no centro de rastreamento de cédulas. |
| **Usar funcionalidade avançada de auditoria** | Ao ativar essa opção, os eleitores terão acesso a funcionalidades que permitiriam auditar a eleição, contudo deixa a interface um pouco mais complicada. |
| **Tornar ordem das questões aleatória**       | Habilite essa opção se você quiser que as questões apareçam em ordem aleatória para cada eleitor |
| **Privada?**                                  | Uma eleição privada só é visível para eleitores registrados manualmente. Se ativado, os eleitores receberão em seus emails uma identificação(ID) e senha aleatória a serem utilizados para a votação. |
| **Endereço de email para ajuda**              | Informe um endereço de email que os eleitores devem usar caso precisem de alguma ajuda sobre o processo de eleição. |
| **Votação começa em**                         | Data e horário de início da votação, em que os eleitores podem depositar o voto na urna. Ainda assim é necessário que o Gestor da Eleição Congele as Cédulas e Abra a Eleição. |
| **Votação termina em**                        | Data e horário de término da votação, onde não será mais possível depositar cédulas na urna. É possível estender o prazo de término em uma eleição em andamento. |

![Criar eleição](/img/gestor-criar.gif)

## Adicionar questões :id=adicionar-questoes

Na página inicial da eleição clique no botão **Questões**.  É possível adicionar quantas questões forem necessárias.

- **Uma eleição** é composta por uma ou mais questões.
- **Uma questão** é composta por uma ou mais respostas.
  - É necessário que indique o **número mínimo e o número máximo de respostas** que o eleitor poderá escolher.
  - Por padrão aparecem campos para 5 respostas. Você pode preencher menos que 5, contudo se precisar mais que 5, então clique no *link* **Adicionar mais 5 respostas**.
- Cada resposta pode ter (opcional), além do texto principal, o endereço de uma página web externa contendo informações adicionais o candidato(a).

> Para contabilizar votos Nulos e/ou Brancos, adicione estas opções manualmente nas repostas da questão

Após preencher as questões e respostas clique em **Adicionar questão** e em **Voltar para a eleição** no topo da página, é possível editar as questão antes da cédula ser **Congelada**

![Adicionar questões](/img/gestor-questoes.gif)

## Carregar lista de eleitore(a)s :id=carregar-lista-eleitores

Na página inicial da eleição clique no botão **Eleitores & Cédulas**, selecione a opção **Apenas eleitores do mesmo sistema** e em **Atualizar** para salvar a configuração.

!> A opção **Apenas eleitores do mesmo sistema** permite que os eleitore(a)s utilizem o login e senha padrão para os sistemas utilizados no *campus* Canoas

Só poderão votar em uma eleição os eleitores que forem carregados por meio de um arquivo CSV. Cada linha do arquivo do CSV representa um único eleitor e é composta pelos seguintes campos:

 `login, endereço-de-email,Nome Completo`. 

Exemplo:
```
11122233344,eleitor@email.com,Eleitor Nonono
12345678900,segundo@email.com,Segundo Nomomom
```

O arquivo deve conter:
* um eleitor por linha (não deve conter título da coluna)
* estar no formato UTF-8
* cada linha com três informações separadas por vírgulas, na seguinte ordem: ID, E-mail e Nome Completo


!>A validação de IDs repetidos deve ser feita antes da carga. Não é feita a verificação de IDs repetidos. Caso isso ocorra o Helios indicará sucesso, porém só permanecerá na base o primeiro registro carregado e o número de eleitores da urna irá ser menor da quantidade de registros carregados (quantidade de linhas do(s) arquivo(s) carregado(s))

### Passo a passo para carregar a lista :id=carregar-lista-eleitores-passos
1. Clique no botão **carregar arquivo de eleitores**
2. Na próxima página clique no botão para procurar o arquivo CSV no disco do seu computador e por fim clique no botão **Carregar**.
3. Será apresentada uma prévia do arquivo ( primeiras 5 linhas) que você está prestes a carregar. Se estiver correto, então clique no botão **Sim, carregar**. Se deseja carregar outro arquivo, então clique no botão **Não, deixe-me carregar um arquivo diferente**. 

>Este o progresso ocorre em segundo plano e depende do tamanho do arquivo que fora carregado, atualize a página (pressionando o botão **Atualizar** do navegador *web* ou a tecla F5) para verificar o progresso do processamento do arquivo. 

Ao término do processamento os eleitores serão exibidos na tela e...
* Serão exibidos 50 eleitores por vez, utilize os links Próximo e Anterior para navegar
* Se a votação tiver mais de 20 eleitores, será habilitado um campo de pesquisa de eleitores. 
* Repita o processo de carregamento de eleitores até que todos estejam na votação.
* O carregamento de eleitores também é possível durante a votação

Por fim, clique no link **Voltar para a eleição** que está no topo da página.

![Adicionar eleitores](/img/gestor-eleitores.gif)


## Iniciar votação :id=iniciar-votacao

### Congelar cédulas e abrir a eleição :id=congelar-cedulas

Após cadastradas as questões e eleitore(a)s certifique-se que esta tudo correto e clique em **Congelar cédula e abrir eleição**, confirme o procedimento clicando no botão **Congelar a cédula**

!>Após o congelamento da cédula informações como questões, apuradores e outros detalhes não poderão ser alterados

Pronto, neste momento os eleitores já estão aptos a votarem a partir da data e horário definidos no momento da [criação da eleição](#criar-eleicao)

![Congelar eleição](/img/gestor-congelar.gif)


### Divulgar a eleição :id=divulgar-eleicao

Ao divulgar o link da eleição coloque o sufixo **/vote** no final da **URL da Eleição**, utilize este link para divulgar no próprio edital e/ou no site do *campus*.

Exemplo: https://vote.canoas.ifrs.edu.br/helios/e/cor-preferida_1/vote

> Para dar mais segurança ao eleitor opcionalmente divulgue também o **Código de Identificação da Eleição**, este código é único e também é mostrado na cabine de votação para o eleitor.


!> Não utilize a função de envios de e-mail do sistema. No formato de eleição recomendado utilizar o sistema de envio de e-mails irá confundir os eleitores e irá compremeter o servidor de e-mails do campus.

![Divulgar eleição](/img/gestor-divulgar-eleicao.gif)


## Encerrar eleição :id=encerrar-eleicao

Ao terminar o período de votação, siga os passos para iniciar o processo de apuração e divulgação dos votos.

!> Após fechar uma eleição, nenhum eleitor poderá mais colocar votos na urna. Esse processo não pode ser desfeito.

### Passo a passo :id=encerrar-eleicao-passos
1. Na página inicial da eleição clique no link **Iniciar a apuração dos votos, ninguém mais poderá votar** e em seguida **Computar apuração criptografada**, você será redirecionado para a página inicial da eleição informando que a apuração está a caminho.

>Este o progresso ocorre em segundo plano, atualize a página (pressionando o botão **Atualizar** do navegador *web* ou a tecla F5) para verificar o processo.

2. Quando a apuração terminar, na página inicial da eleição clique no link **Computar o resultado**. 
3. Em seguda clique no botão **Computar a apuração!** e você será redirecionado para a página inicial da eleição e no final desta página estará o resultado, que no momento, **somente você conseguirá ver**.
4. Clique no link **Liberar o resultado**, desmarque a opção **Enviar e-mail para eleitores** e novamente clique em **Liberar o resultado** para que esse resultado fique público na página da eleição.

![Apuração eleição](/img/gestor-apuracao.gif)


### Divulgar o resultado :id=divulgar-resultado

Ao divulgar o resultado da eleição use o link exatamente como informado em **URL da Eleição**, utilize este link para divulgar o resultado no site do *campus*.

Exemplo: https://vote.canoas.ifrs.edu.br/helios/e/cor-preferida_1

Pronto, todo o fluxo da eleição esta encerrado.

## Solicitar permissão :id=solicitar-permissao

Para solicitar permissões de gestor abra um chamado via [Sistema de Chamados de Suporte](https://suporte.canoas.ifrs.edu.br/)

